﻿using Microsoft.AspNetCore.Mvc;
using MovieApi.Exceptions;
using MovieApi.Models;
using MovieApi.Repository;
using System;

namespace MovieApi.Controllers
{
    public class MovieController : ControllerBase
    {
        private readonly IMovieRepository movieRepository;

        public MovieController(IMovieRepository movieRepository)
        {
            this.movieRepository = movieRepository;
        }

        //Hämta alla filmer
        [HttpGet]
        [Route("api/movie/list")]
        public IActionResult GetAllMovies(string query = "")
        {
            try
            {
                return Ok (movieRepository.GetMovies(query));
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        //Hämta en film
        [HttpGet]
        [Route("api/movie/{id}")]
        public IActionResult GetMovie(int id)
        {
            try
            {
                var movie = movieRepository.GetMovie(id);

                if (movie == null)
                {
                    return NotFound();
                }
                return Ok(movie);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
           
        }

        //Lägg till en film
        [HttpPost]
        [Route("api/movie/add")]
        public IActionResult Add([FromBody] Movie movie)
        {
            try
            {
                movieRepository.Add(movie);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        //Uppdatera
        [HttpPut]
        [Route("api/movie/update")]
        public IActionResult Update([FromBody] Movie movie)
        {
            try
            {
                movieRepository.Update(movie);

                return Ok();
            }
            catch (MovieNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }     
        }

        //Ta bort
        [HttpDelete]
        [Route("api/movie/delete/{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                movieRepository.Delete(id);

                return Ok();
            }
            catch(MovieNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

    }
}
