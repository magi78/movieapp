﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Text;
using MovieApi.Exceptions;
using MovieApi.Models;

namespace MovieApi.Repository
{
    public class SQLMovieRepository : IMovieRepository
    {
        private readonly string connectionString;

        public SQLMovieRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Add(Movie movie)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("INSERT INTO [Movies].[dbo].[Movies] ");
                sb.Append("VALUES (@title, @category, @poster) ");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@title", movie.Title);
                    if (!string.IsNullOrEmpty(movie.Category))
                    {
                        command.Parameters.AddWithValue("@category", movie.Category);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@category", DBNull.Value);
                    }

                    if (!string.IsNullOrEmpty(movie.PosterUrl))
                    {
                        command.Parameters.AddWithValue("@poster", movie.PosterUrl);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@poster", DBNull.Value);
                    }

                    if (command.ExecuteNonQuery() < 1)
                    {
                        throw new Exception("Could not add movie");
                    }
                }
            }
        }

        public IEnumerable<Movie> GetMovies(string query)
        {
            bool useQuery = !string.IsNullOrEmpty(query);

            List<Movie> movies = new List<Movie>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("SELECT TOP 1000 [Id], [Title], [Category], [Poster] ");
                stringBuilder.Append("FROM [Movies].[dbo].[Movies] ");
                if (useQuery)
                {
                    stringBuilder.Append("WHERE [Title] like '%' + @query + '%' ");
                }
                String sql = stringBuilder.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();

                    if (useQuery)
                    {
                        command.Parameters.AddWithValue("@query", query);
                    }

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Movie movie = new Movie
                            {
                                Id = reader.GetInt32(0),
                                Title = reader.GetString(1),
                                Category = reader.IsDBNull(2) ? null : reader.GetString(2),
                                PosterUrl = reader.IsDBNull(3) ? null : reader.GetString(3)
                            };

                            movies.Add(movie);
                        }
                    }
                }
            }
            return movies;
        }

        public Movie GetMovie(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT [Id], [Title], [Category], [Poster] ");
                sb.Append("FROM [Movies].[dbo].[Movies]");
                sb.Append("WHERE Id = @id ");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Movie movie = new Movie
                            {
                                Id = reader.GetInt32(0),
                                Title = reader.GetString(1),
                                Category = reader.IsDBNull(2) ? null: reader.GetString(2),
                                PosterUrl = reader.IsDBNull(3) ? null: reader.GetString(3)

                            };

                            return movie;

                        }
                    }
                }
            }
            return null;
        }


        public void Update(Movie movie)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("UPDATE [Movies].[dbo].[Movies] ");
                sb.Append("SET [Title] = @title, [Category] = @category, [Poster] = @poster ");
                sb.Append("WHERE Id = @id ");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@title", movie.Title);
                    command.Parameters.AddWithValue("@id", movie.Id);
                    if (!string.IsNullOrEmpty(movie.Category))
                    {
                        command.Parameters.AddWithValue("@category", movie.Category);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@category", DBNull.Value);
                    }

                    if (!string.IsNullOrEmpty(movie.PosterUrl))
                    {
                        command.Parameters.AddWithValue("@poster", movie.PosterUrl);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@poster", DBNull.Value);
                    }

                    if (command.ExecuteNonQuery() < 1)
                    {
                        throw new MovieNotFoundException($"Could not find movie {movie.Id}");
                    }

                }
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("DELETE FROM [Movies].[dbo].[Movies] ");
                sb.Append("WHERE Id = @Id");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@Id", id);

                    if (command.ExecuteNonQuery() < 1)
                    {
                        throw new MovieNotFoundException($"Could not find movie {id}");
                    }
                }
            }
        }
    }
}