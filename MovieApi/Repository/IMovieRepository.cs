﻿using MovieApi.Models;
using System.Collections.Generic;

namespace MovieApi.Repository
{
    public interface IMovieRepository
    {
        Movie GetMovie(int id);
        IEnumerable<Movie> GetMovies(string query);
        void Add(Movie movie);
        void Update(Movie movie);
        void Delete(int id);
    }
}
