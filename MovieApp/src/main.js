import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import Home from './components/Home.vue';
import AddMovie from './components/AddMovie.vue';
import EditMovie from './components/EditMovie.vue';

Vue.use(VueRouter);

Vue.config.productionTip = true;

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/', component: Home },
        { path: '/home', component: Home },
        { path: '/addmovie', component: AddMovie },
        { path: '/editmovie/:id', name: 'editmovie', component: EditMovie, props: true }
    ]
});

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
